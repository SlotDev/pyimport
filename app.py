import xlrd
import mysql.connector
import datetime

book = xlrd.open_workbook("STUDENT_CU_60RFID.xls")
# sheet =  book.sheet_by_name("source")
sheet = book.sheet_by_index(0)

database = mysql.connector.connect(
    host="localhost",
    user="admindb",
    passwd="adminpass",
    database="testdb"
)


def formatNumber(num):
    if num % 1 == 0:
        return int(num)
    else:
        return num


cursor = database.cursor()
print(cursor)
print(database)

date = "2019-11-20"
today = datetime.datetime.strptime(date, "%Y-%m-%d")
print(today)
for r in range(1, sheet.nrows):
    cardout = sheet.cell(r, 11)
    # print(datetime.strptime.cardout)

query = "INSERT INTO access_card (STUDENTCODE, ID, RFID, NAMETHAI, COUNT, COMP_ID, GROUP_ID, POSITION_ID, FACULTY, DEPNAME, MARJORNAME, CARDOUT, CARDEXPIRE) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
for r in range(1, sheet.nrows):
    StudentCode = sheet.cell(r, 0).value
    Id = sheet.cell(r, 1).value
    RFID = sheet.cell(r, 2).value
    NameThai = sheet.cell(r, 3).value
    Count = sheet.cell(r, 4).value
    CompId = sheet.cell(r, 5).value
    GroupId = sheet.cell(r, 6).value
    PositionId = sheet.cell(r, 7).value
    Faculty = sheet.cell(r, 8).value
    Depname = sheet.cell(r, 9).value
    MarjorName = sheet.cell(r, 10).value
    CardOut = sheet.cell(r, 11).value
    CardExpire = sheet.cell(r, 12).value

    values = (StudentCode, Id, RFID, NameThai, Count, CompId, GroupId, PositionId,
              Faculty, Depname, MarjorName, CardOut, CardExpire)

    cursor.execute(query, values)

cursor.close()
database.commit()
database.close()

columns = str(sheet.ncols)
rows = str(sheet.nrows)
# print "PyImport "+columns+" columns and "+rows+" rows to MySQL!"
